# NGO MEMBERSHIP MANAGEMENT #

### What is this repository for? ###

* A module to manage in Odoo 9.0 special partners that are members or donors of the ngo

### How do I get set up? ###

#### Requirements ####

* Same requirements as for Odoo v9.0

#### Istructions ####

* fork this repo and clone the fork
* add cloned root folder in addons_path in openerp-server.conf
* restart openerp-server.py
* You can restore a previous db (if you have any) or start from an EMPTY DB
    * create new DB
    * go to Settings -> Update Module List
    * go to Settings -> Installed Module 
    * install ngo_membership_management


### Contribution guidelines ###

* Fork, develop & pull-request
* Double analyze the existing code, don't write what is already written, be DRY (Don't Repeat Yourself)
* Dumb code is better than clever one when is time to share
* Write comments when only "YOU" know what you are doing
* Optimize only after achieved
* Less is more!

### Who do I talk to? ###

* Repo owner or admin: [Giorgio Mantovani](https://bitbucket.org/giorgio_mantovani)
* Other community or team contact: [ISF - Informatici Senza Frontiere ONLUS](http://www.informaticisenzafrontiere.org/)