# -*- coding: utf-8 -*-

from datetime import datetime
from openerp import models, fields, api, exceptions, _
from openerp.exceptions import  Warning

#Import logger
import logging

#Import os
import os

#Import PyPDF2
import PyPDF2
from PyPDF2 import PdfFileWriter, PdfFileReader #, PdfFileMerger
from PyPDF2.generic import BooleanObject, NameObject, IndirectObject

#Get the logger
_logger = logging.getLogger(__name__)

# Define RECEIPT TEMPLATE AND REACEIPT Path
#RECEIPT_TEMPLATE_PATH = r'/usr/lib/python2.7/dist-packages/openerp/custom_addons/ngo_membership_management/views/receipt_template.pdf'
#RECEIPT_OUTPUT_PATH = r'/usr/lib/python2.7/dist-packages/openerp/custom_addons/ngo_membership_management/views/receipt.pdf'

def set_need_appearances_writer(writer=PdfFileWriter):
# See 12.7.2 and 7.7.2 for more information: http://www.adobe.com/conten...
    try:
        catalog = writer._root_object
        # get the AcroForm tree
        if "/AcroForm" not in catalog:
            writer._root_object.update({
              NameObject("/AcroForm"): IndirectObject(len(writer._objects), 0, writer)})
            need_appearances = NameObject("/NeedAppearances")
            writer._root_object["/AcroForm"][need_appearances] = BooleanObject(True)
            return writer
    except Exception as e:
        print('set_need_appearances_writer() catch : ', repr(e))
        return writer

# Define Receipt Number
def _newreceipt(self):
    today = fields.Date.today()
    todayyear,todaymonth,todayday = today.split("-")
    lastreceipt = self.env['ngo_membership_management.receipt'].search([])[-1].number 
    rcptyear, rcptseq = lastreceipt.split("/")
    lastdonation = self.env['ngo_membership_management.donation'].search([])[-1].number 
    donyear, donseq = lastdonation.split("/")
    if int(rcptyear) > int(donyear):
        year = rcptyear
        seq = rcptseq
        _logger.debug("int(rcptyear) > int(donyear)")
    elif int(rcptyear) < int(donyear):
        year = donyear
        seq = donseq
        _logger.debug("int(rcptyear) < int(donyear)")
    else:
        _logger.debug("int(rcptyear) = int(donyear)")
        year = rcptyear
        seq = str(max(int(rcptseq), int(donseq)))
        _logger.debug("rcptseq = " + rcptseq + " - donseq = " +donseq)
    if int(todayyear) > int(year):
        newreceipt = todayyear + "/" + "0001"
    else:
        newreceipt = year + "/" + str(int(seq) + 1).zfill(4)
    return(newreceipt)

#Create receipt for member and donor
def build_member_receipt(self):
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)))
    RECEIPT_TEMPLATE_PATH = path.replace("models","views") + '/receipt_template.pdf'
    RECEIPT_OUTPUT_PATH = self.env['ngo_membership_management.receipts_folder'].search([])[-1].receipts_folder + 'receipt.pdf'
    for r in self:
        today = fields.Date.today()
        todayyear,todaymonth,todayday = today.split("-")
        issuedate = todayday + "/" + todaymonth + "/" + todayyear
        
        # Member receipt
        if r.member:
            if not r.m_rcptdelivred:
                _logger.debug("RICEVUTA SOCIO - INIZIO")
                if not r.amount > 0.00:
                    raise Warning("Invalid amount")
                r.receiptno = _newreceipt(self)
                amount1 = 'Con la presente riscontriamo il versamento di Euro ' + str(r.amount) + ' a titolo quota associativa'
                amount2 = amount1 + '\r'
                amount3 = amount2 + '\n'
                if r.valyear > todayyear: 
                    amount = amount3 + "Informatici Senza Frontiere per gli anni " + todayyear + "/" + r.valyear
                else:
                    amount = amount3 + "Informatici Senza Frontiere per l'anno " + r.valyear
                if r.vat==False:
                    raise Warning("Invalid Tax Code")
                else:
                    vat = 'Codice Fiscale: ' + r.vat
                if r.street==False:
                    raise Warning("Address is missing")
                else:
                    street = r.street
                if r.zip==False:
                    raise Warning("ZIP is missing")
                else:
                    zip = r.zip
                if r.city==False:
                    raise Warning("Address incomplete")
                else:
                    city = r.city
                if r.state_id==False:
                    raise Warning("State ID is missing")
                else:
                    state_id = r.state_id
                data_dict = {
                   'issuedate': issuedate,
                   'salutation': 'Egr. Sig.',
                   'taxcode': vat,
                   'name': r.name,
                   'streetadr': street,
                   'zip': zip,
                   'city': city,
                   'county': state_id,
                   'receipt': 'Ricevuta n. ' + r.receiptno + ' del ' + issuedate,
                   'amount': amount.encode(encoding='ascii',errors='ignore'),
                }
                reader = PdfFileReader(open(RECEIPT_TEMPLATE_PATH, "rb"), strict=False)
                if "/AcroForm" in reader.trailer["/Root"]:
                    reader.trailer["/Root"]["/AcroForm"].update({NameObject("/NeedAppearances"): BooleanObject(True)})
                    writer = PdfFileWriter()
                    set_need_appearances_writer(writer)
                if "/AcroForm" in writer._root_object:
                    writer._root_object["/AcroForm"].update({NameObject("/NeedAppearances"): BooleanObject(True)})
                
                writer.addPage(reader.getPage(0))
                writer.updatePageFormFieldValues(writer.getPage(0), data_dict)
                
                aString = "receipt" + r.receiptno.replace("/",".")
                outString = RECEIPT_OUTPUT_PATH.replace("receipt",aString)
                outputStream = open(outString, "wb")
                writer.write(outputStream)
                outputStream.close
                r.m_rcptdelivred = True
                self.env['ngo_membership_management.receipt'].create(
                   {
                        'number': r.receiptno,
                        'member': r.id,
                        'amount': r.amount,
                        'issuedate': issuedate                }
                )
                _logger.debug("RICEVUTA SOCIO - FINE")
                return outString, r.receiptno, r.id
            else:
                return '0', '0', '0'
        else:
            return '0', '0', '0'

def build_donor_receipt(self):
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)))
    RECEIPT_TEMPLATE_PATH = path.replace("models","views") + '/receipt_template.pdf'
    RECEIPT_OUTPUT_PATH = self.env['ngo_membership_management.receipts_folder'].search([])[-1].receipts_folder + 'receipt.pdf'
    for r in self:
        today = fields.Date.today()
        todayyear,todaymonth,todayday = today.split("-")
        issuedate = todayday + "/" + todaymonth + "/" + todayyear

        # Donor receipt
        if r.donor:
            if not r.d_rcptdelivred:
                if not r.donamount > 0.00:
                    raise Warning("Invalid amount")
                _logger.debug("RICEVUTA DONATORE - INIZIO")
                r.donreceiptno = _newreceipt(self)
                amount1 = 'Con la presente riscontriamo il versamento di Euro ' + str(r.donamount) + ' come erogazione liberale in'
                amount2 = amount1 + '\r'
                amount3 = amount2 + '\n'
                amount = amount3 + "denaro a sostegno delle attivita' di Informatici Senza Frontiere."
                if r.vat==False:
                    raise Warning("Invalid Tax Code")
                else:
                    vat = 'Codice Fiscale: ' + r.vat
                if r.street==False:
                    raise Warning("Address is missing")
                else:
                    street = r.street
                if r.zip==False:
                    raise Warning("ZIP is missing")
                else:
                    zip = r.zip
                if r.city==False:
                    raise Warning("Address incomplete")
                else:
                    city = r.city
                if r.state_id==False:
                    raise Warning("State ID is missing")
                else:
                    state_id = r.state_id
                data_dict = {
                   'issuedate': issuedate,
                   'salutation': 'Egr. Sig.',
                   'taxcode': vat,
                   'name': r.name,
                   'streetadr': street,
                   'zip': zip,
                   'city': city,
                   'county': state_id,
                   'receipt': 'Ricevuta n. ' + r.donreceiptno + ' del ' + issuedate,
                   'amount': amount.encode(encoding='ascii',errors='ignore'),
                }
                reader = PdfFileReader(open(RECEIPT_TEMPLATE_PATH, "rb"), strict=False)
                if "/AcroForm" in reader.trailer["/Root"]:
                    reader.trailer["/Root"]["/AcroForm"].update({NameObject("/NeedAppearances"): BooleanObject(True)})
                    writer = PdfFileWriter()
                    set_need_appearances_writer(writer)
                if "/AcroForm" in writer._root_object:
                    writer._root_object["/AcroForm"].update({NameObject("/NeedAppearances"): BooleanObject(True)})
                
                writer.addPage(reader.getPage(0))
                writer.updatePageFormFieldValues(writer.getPage(0), data_dict)
                
                aString = "receipt" + r.donreceiptno.replace("/",".")
                outString = RECEIPT_OUTPUT_PATH.replace("receipt",aString)
                outputStream = open(outString, "wb")
                writer.write(outputStream)
                outputStream.close
                r.d_rcptdelivred = True
                self.env['ngo_membership_management.donation'].create(
                   {
                        'number': r.donreceiptno,
                        'member': r.id,
                        'amount': r.amount,
                        'issuedate': issuedate                }
                )
                _logger.debug("RICEVUTA DONATORE - FINE")
                return outString, r.donreceiptno, r.id
            else:
                return '0', '0', '0'
        else:
            return '0', '0', '0'
            

def store_receipt(self, outString, receiptno, id):
    pdf = open(outString,'rb').read().encode('base64')
    attachment = {'name': receiptno,
                  'type':'binary',    
                  'datas': pdf, 
                  'datas_fname': outString, 
                  'res_model': 'res.partner',
                  'res_id': id,
                  'mimetype': 'application/x-pdf'}
    self.env['ir.attachment'].create(attachment)
    

# Modify res.partner class
class res_partner(models.Model):
    _name = 'res.partner'

    _inherit = 'res.partner'

    # Add a new column to the res.partner model, by default partners are not
    # members or donors
    gender = fields.Selection(selection=[('M','Male'), ('F', 'Female')])
    birthplace = fields.Char(string="Birth place")
    iban = fields.Char(string="IBAN", size=27)
    groupemail = fields.Char(string="Group email")
    skype = fields.Char(string="Skype")
    domainemail = fields.Char(string="Domani email")
    member = fields.Boolean(string="Is Member", default=False)
    lastpayment = fields.Date(string="Last Renewal", default="1900-01-01")
    donor = fields.Boolean(string="Is Donor", default=False)
    lastdonation = fields.Date(string="Last Donation", default="1900-01-01")
    registrationdate = fields.Date(string="Registration Date")
    resignationdate = fields.Date(string="Resignation Date")
    section = fields.Many2one('ngo_membership_management.section',ondelete='set null', string="Section", index=True)
    abroad = fields.Boolean(string="Missons abroad")
    homeland = fields.Boolean(string="Missons in homeland")
    skills = fields.Text()
    receiptno = fields.Char(string="Receipt Number")
    amount = fields.Float(string="Last payment amount", digits=(2,2))
    donamount = fields.Float(string="Last donation amount", digits=(2,2))
    donreceiptno = fields.Char(string="Receipt Number")
    valyear = fields.Char(string="Validity Year", size=4, compute='_valyear', store=True)
    resdate = fields.Date(string="Resignation date")
    expirdate = fields.Date(string="Expiration Date", compute='_expirdate', store=True)
    onrenewal = fields.Boolean(string="To renew", default = False)
    last_mail = fields.Date(string="Last renewal mail on")
    mail_counter = fields.Integer(string="No of sent mails", compute='_mail_counter', default = 0, store=True)
    valid_membership = fields.Boolean(string="Valid Membership", compute='_expirdate', default = True, store=True)
    m_rcptdelivred = fields.Boolean(string="Reciept Delivered", compute='_m_rcptdelivred', default=True, store=True)
    d_rcptdelivred = fields.Boolean(string="Reciept Delivered", compute='_d_rcptdelivred', default=True, store=True)
    receipts = fields.One2many('ngo_membership_management.receipt', 'member', string="Receipts" )
    donations = fields.One2many('ngo_membership_management.donation', 'member', string="Donations" )
    
    @api.multi
    def copy(self, default=None):
        default = dict(default or {})

        copied_count = self.search_count(
            [('name', '=like', _(u"Copy of {}%").format(self.name))])
        if not copied_count:
            new_name = _(u"Copy of {}").format(self.name)
        else:
            new_name = _(u"Copy of {} ({})").format(self.name, copied_count)

        default['name'] = new_name
        return super(res_partner, self).copy(default)

#    lastpayment dependencies

#    expirdate
    @api.depends('lastpayment')
    def _expirdate(self):
        _logger.debug("Expidate START")
        for r in self:
            year,month,day = r.lastpayment.split("-")
            if int(month) > 9:
                expirdate = str(int(year)+1) + "-12-31"
                r.expirdate = fields.Datetime.from_string(expirdate)
            else:
                expirdate = year + "-12-31"
                r.expirdate = fields.Datetime.from_string(expirdate)
            _logger.debug("Expirdate prima di Valid Membership")
            r.valid_membership = True
            _logger.debug(r.valid_membership)
            _logger.debug("Expirdate dopo Valid Membership")

#    valyear
    @api.depends('lastpayment')
    def _valyear(self):
        for r in self:
            year,month,day = r.expirdate.split("-")
            r.valyear=year
            r.customer = True

#    mail_counter
    @api.depends('lastpayment')
    def _mail_counter(self):
        for r in self:
            r.mail_counter = 0

#    _m_rcptdelivred
    @api.depends('lastpayment')
    def _m_rcptdelivred(self):
        for r in self:
            r.m_rcptdelivred = False

#    lastdonation dependencies

#    _d_rcptdelivred
    @api.depends('lastdonation')
    def _d_rcptdelivred(self):
        for r in self:
            r.d_rcptdelivred = False
            r.customer = True
            
#    Create receipt for member and donor and store in attachments
    @api.one
    def build_store_receipt(self):
        outString, receiptno, id = build_member_receipt(self)
        if not outString == '0':
            store_receipt(self, outString, receiptno, id)
        outString, receiptno, id = build_donor_receipt(self)
        if not outString == '0':
            store_receipt(self, outString, receiptno, id)
        
    def check_expiration_date(self, cr, uid, context=None):
        _logger.debug("Check_expiration_date START")
        member_line_obj = self.pool.get('res.partner')
        #Contains all ids for the model scheduler.demo
        member_line_ids = self.pool.get('res.partner').search(cr, uid, [])   
        #Loops over every record in the model scheduler.demo
        for member_line_id in member_line_ids :
            #Contains all details from the record in the variable member_line
            member_line =member_line_obj.browse(cr, uid,member_line_id ,context=context)
            if member_line.member:
                valid_membership = member_line.valid_membership
                onrenewal = False
                msg = "onrenewal = False"
                if valid_membership:
                    today = fields.Date.today()
                    expirdate = member_line.expirdate
                    todayyear,todaymonth,todayday = today.split("-")
                    expiryear,expirmonth,expirday = expirdate.split("-")
                    # This is the correct line
#                    if (int(todayyear) > int(expiryear)) and (int(todaymonth) < 5):
                    # This is the dubug line to DELETE for production
                    if (int(todayyear) > int(expiryear)) and (int(todaymonth) < 12):

                        onrenewal = True
                        msg = "onrenewal = True"
                    _logger.debug(msg)
                    #Update the record
                member_line_obj.write(cr, uid, member_line_id, {'onrenewal': (onrenewal)}, context=context)
        _logger.debug("Check_expiration_date END")

    @api.multi
    def check_expiration_date_new(self):
        _logger.debug("Check_expiration_date START")
        for r in self:
            if r.member:
                onrenewal = False
                msg = "onrenewal = False"
                if r.valid_membership:
                    today = fields.Date.today()
                    expirdate = r.expirdate
                    todayyear,todaymonth,todayday = today.split("-")
                    expiryear,expirmonth,expirday = expirdate.split("-")
                    # This is the correct line
#                    if (int(todayyear) > int(expiryear)) and (int(todaymonth) < 5):
                    # This is the dubug line to DELETE for production
                    if (int(todayyear) > int(expiryear)) and (int(todaymonth) < 12):
                        onrenewal = True
                        msg = "onrenewal = True"
                _logger.debug(msg)
                #Update the record
                r.onrenewal = onrenewal
        _logger.debug("Check_expiration_date END")

    @api.multi
    def send_mail_to_member(self, mail_template):
        _logger.debug("send_mail_to_member START")
        # Find the e-mail template
        # template = self.env.ref('ngo_membership_management.membership_renewal')
        # You can also find the e-mail template like this:
        template = self.env['ir.model.data'].get_object('ngo_membership_management', mail_template)
 
        # Send out the e-mail template to the user
        self.env['mail.template'].browse(template.id).send_mail(self.id)
        _logger.debug("send_mail_to_member END")
        
    def expiration_mail_send(self, cr, uid, context=None):
        _logger.debug("expiration_mail_send START")
        member_line_obj = self.pool.get('res.partner')
        _logger.debug("expiration_mail_send Got member_line_obj")
        #Contains all ids for the model scheduler.demo
        member_line_ids = self.pool.get('res.partner').search(cr, uid, [])
        _logger.debug("expiration_mail_send Got member_line_ids")   
        #Loops over every record in the model scheduler.demo
        for member_line_id in member_line_ids:
            member_line = member_line_obj.browse(cr, uid,member_line_id ,context=context)
            _logger.debug("expiration_mail_send Got member_line")
            if member_line.member:
                _logger.debug("expiration_mail_send Is a member")
                mail_counter = 0
                onrenewal=member_line.onrenewal
                if onrenewal:
                    _logger.debug("expiration_mail_send Is om renewal")
                    mail_counter = member_line.mail_counter
                    today = fields.Date.today()
                    valid_membership = member_line.valid_membership
                    if (mail_counter == 0):
                        _logger.debug("expiration_mail_send Mail conter is 0")
                        _logger.debug("expiration_mail_send Before Call send_mail")
                        res_partner.send_mail_to_member(member_line, 'membership_renewal')
                        _logger.debug("expiration_mail_send After Call send_mail")
                        mail_counter = mail_counter + 1
                        last_mail = today
                        _logger.debug("First mail")
                    else:
                        _logger.debug("expiration_mail_send Mail conter is >0")
                        date_format = '%Y-%m-%d'
                        current_date = (datetime.today()).strftime(date_format)
                        last_mail = fields.Datetime.from_string(member_line.last_mail)
                        d1 = datetime.strptime(member_line.last_mail, date_format).date()
                        d2 = datetime.strptime(current_date, date_format).date()
                        difference = (d2 - d1).days
                        _logger.debug(difference)
                        if (difference >= 28):
                            mail_counter = mail_counter + 1
                            last_mail = today
                            _logger.debug("MAIL No " + str(mail_counter))
                            _logger.debug("expiration_mail_send Before Call send_mail")
                            res_partner.send_mail_to_member(member_line, 'membership_renewal')
                            _logger.debug("expiration_mail_send After Call send_mail")
                            if mail_counter == 4:
                                valid_membership = False
                                onrenewal = False
                    member_line_obj.write(cr, uid, member_line_id, {'mail_counter': (mail_counter)}, context=context)
                    member_line_obj.write(cr, uid, member_line_id, {'last_mail': (last_mail)}, context=context)
                    member_line_obj.write(cr, uid, member_line_id, {'valid_membership': (valid_membership)}, context=context)
                    member_line_obj.write(cr, uid, member_line_id, {'onrenewal': (onrenewal)}, context=context)
        _logger.debug("expiration_mail_send END")

    @api.multi
    def expiration_mail_send_new(self):
        _logger.debug("expiration_mail_send START")
        #Loops over every record in the model scheduler.demo
        for r in self:
            _logger.debug("expiration_mail_send Got member_line")
            if r.member:
                _logger.debug("expiration_mail_send Is a member")
                mail_counter = 0
                onrenewal=r.onrenewal
                if onrenewal:
                    _logger.debug("expiration_mail_send Is om renewal")
                    mail_counter = r.mail_counter
                    today = fields.Date.today()
                    valid_membership = r.valid_membership
                    if (mail_counter == 0):
                        _logger.debug("expiration_mail_send Mail conter is 0")
                        _logger.debug("expiration_mail_send Before Call send_mail")
                        res_partner.send_mail_to_member(r, 'membership_renewal')
                        _logger.debug("expiration_mail_send After Call send_mail")
                        mail_counter = mail_counter + 1
                        last_mail = today
                        _logger.debug("First mail")
                    else:
                        _logger.debug("expiration_mail_send Mail conter is >0")
                        date_format = '%Y-%m-%d'
                        current_date = (datetime.today()).strftime(date_format)
                        last_mail = fields.Datetime.from_string(r.last_mail)
                        d1 = datetime.strptime(r.last_mail, date_format).date()
                        d2 = datetime.strptime(current_date, date_format).date()
                        difference = (d2 - d1).days
                        _logger.debug(difference)
                        if (difference >= 28):
                            mail_counter = mail_counter + 1
                            last_mail = today
                            _logger.debug("MAIL No " + str(mail_counter))
                            _logger.debug("expiration_mail_send Before Call send_mail")
                            res_partner.send_mail_to_member(r, 'membership_renewal')
                            _logger.debug("expiration_mail_send After Call send_mail")
                            if mail_counter == 4:
                                valid_membership = False
                                onrenewal = False
                    r.mail_counter = mail_counter
                    r.last_mail = last_mail
                    r.valid_membership = valid_membership
                    r.onrenewal = onrenewal
        _logger.debug("expiration_mail_send END")

# Class Section        
class Section(models.Model):
    _name = 'ngo_membership_management.section'
    
    name = fields.Char(string="Section", required=True)
    coordinator = fields.Many2one('res.partner', ondelete='set null', string="Coordinator", domain = "[('member','=',True)]", index=True)
    members = fields.One2many('res.partner', 'section', string="Members" )
    nmembers = fields.Integer(string="Number of members")
    openingdate = fields.Date(string="Opening Date")
    street = fields.Char(string="Address")
    zip = fields.Char(striong="Zip", size=24, change_default=True)
    state_id = fields.Many2one("res.country.state", string='State', help='Enter State', ondelete='restrict')
    city =fields.Char(string="City")
    phone = fields.Char(string="Phone")
    note =fields.Text()

# Class Receipt
class Receipt(models.Model):
    _name = 'ngo_membership_management.receipt'
    
    number = fields.Char(string="Number", size=9)
    member = fields.Many2one('res.partner', ondelete='set null', string="Member", domain = "[('member','=',True)]", index=True)
    amount = fields.Float(string="Amount", digits=(2,2), defaul="0.00")
    issuedate = fields.Date(string="Issue Date", default="1900-01-01")

# Class Donation
class Donation(models.Model):
    _name = 'ngo_membership_management.donation'
    
    number = fields.Char(string="Number", size=9)
    member = fields.Many2one('res.partner', ondelete='set null', string="Member", domain = "[('member','=',True)]", index=True)
    amount = fields.Float(string="Amount", digits=(2,2), defaul="0.00")
    issuedate = fields.Date(string="Issue Date", default="1900-01-01")

# Class Receipts_folder
class ngo_membership_management_receipts_folder(models.Model):
    _name = 'ngo_membership_management.receipts_folder'
    
    receipts_folder = fields.Char(string='Receipts Folder', size=128, default="/usr/lib/python2.7/dist-packages/openerp/custom_addons/ngo_membership_management/views/")
    
    @api.model
    def first_folder(self):
        path = os.path.join(os.path.dirname(os.path.abspath(__file__))) + '/'
        newpath = path.replace("models","views")
        self.env['ngo_membership_management.receipts_folder'].create(
           {
                'receipts_folder': newpath                }
        )
    
    @api.onchange('receipts_folder')
    def _check_folder(self):
        for r in self:
            _receipts_folder = r.receipts_folder
            if not os.path.exists(_receipts_folder):
                os.makedirs(_receipts_folder)
            lenght = len(_receipts_folder)-1
            last_char = _receipts_folder[lenght]
            if (last_char != "/"):
                if (last_char != "\\"):
                    r.receipts_folder = _receipts_folder + '/'
