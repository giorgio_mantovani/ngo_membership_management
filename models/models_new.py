# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from openerp import models, fields, api

#Import logger
import logging

#Import pdf writer
import os
import pdfrw

#Get the logger
_logger = logging.getLogger(__name__)

# Class Member
class res_partner(models.Model):
    _name = 'res.partner'

    _inherit = 'res.partner'

    # Add a new column to the res.partner model, by default partners are not
    # members or donors
    gender = fields.Selection(selection=[('M','Male'), ('F', 'Female')])
    birthplace = fields.Char(string="Birth place")
    taxcode = fields.Char(string="Tax code", size=16)
    vatcode = fields.Char(string="VAT code", size=11)
    iban = fields.Char(string="IBAN", size=27)
    groupemail = fields.Char(string="Group email")
    skype = fields.Char(string="Skype")
    domainemail = fields.Char(string="Domani email")
    member = fields.Boolean(string="Member", default=False)
    _logger.debug("Before lastpayment")
    lastpayment = fields.Date(string="Last Renewal", default="1900-01-01")
    _logger.debug("After lastpayment")
    donor = fields.Boolean(string="Donor", default=False)
    lastdonation = fields.Date(string="Last Donation",)
    registrationdate = fields.Date(string="Registration Date")
    resignationdate = fields.Date(string="Resignation Date")
    section = fields.Many2one('ngo_membership_management.section',ondelete='set null', string="Section", index=True)
    abroad = fields.Boolean(string="Missons abroad")
    homeland = fields.Boolean(string="Missons in homeland")
    skills = fields.Text()
    receiptno = fields.Char(string="Receipt Number")
    amount = fields.Float(string="Last payment amount", digits=(2,2))
    valyear = fields.Char(string="Validity Year", size=4, compute='_valyear', store=True)
    resdate = fields.Date(string="Resignation date")
    expirdate = fields.Date(string="Expiration Date", compute='_expirdate', store=True)
    onrenewal = fields.Boolean(string="To renew", default = False)
    last_mail = fields.Date(string="Last renewal mail on")
    mail_counter = fields.Integer(string="No of sent mails", compute='_mail_counter', default = 0, store=True)
    valid_membership = fields.Boolean(string="Valid Membership", compute='_expirdate', default = True, store=True)
    
#Define write pdf function
def write_fillable_pdf(input_pdf_path, output_pdf_path, data_dict):
    template_pdf = pdfrw.PdfReader(input_pdf_path)
    annotations = template_pdf.pages[0][ANNOT_KEY]
    for annotation in annotations:
        if annotation[SUBTYPE_KEY] == WIDGET_SUBTYPE_KEY:
            if annotation[ANNOT_FIELD_KEY]:
                key = annotation[ANNOT_FIELD_KEY][1:-1]
                if key in data_dict.keys():
                    annotation.update(
                        pdfrw.PdfDict(V='{}'.format(data_dict[key]))
                    )
    pdfrw.PdfWriter().write(output_pdf_path, template_pdf)

    @api.multi
    def copy(self, default=None):
        default = dict(default or {})

        copied_count = self.search_count(
            [('name', '=like', _(u"Copy of {}%").format(self.name))])
        if not copied_count:
            new_name = _(u"Copy of {}").format(self.name)
        else:
            new_name = _(u"Copy of {} ({})").format(self.name, copied_count)

        default['name'] = new_name
        return super(Member, self).copy(default)

    @api.depends('lastpayment')
    def _expirdate(self):
        _logger.debug("Expidate START")
        for r in self:
            year,month,day = r.lastpayment.split("-")
            lastpayment = fields.Datetime.from_string(r.lastpayment)
            if int(month) > 9:
                expirdate = str(int(year)+1) + "-12-31"
                r.expirdate = fields.Datetime.from_string(expirdate)
            else:
                expirdate = year + "-12-31"
                r.expirdate = fields.Datetime.from_string(expirdate)
            _logger.debug("Expirdate prima di Valid Membership")
            r.valid_membership = True
            _logger.debug(r.valid_membership)
            _logger.debug("Expirdate dopo Valid Membership")
        
    @api.depends('lastpayment')
    def _valyear(self):
        for r in self:
            _logger.debug("prima _sendreceipt")
#            _sendreceipt(r)
            year,month,day = r.lastpayment.split("-")
            r.valyear=year

    @api.depends('lastpayment')
    def _mail_counter(self):
        for r in self:
            r.mail_counter = 0
            
#    @api.depends('lastpayment')
    def _sendreceipt(self):
        _logger.debug("_sendreceipt")
        #Define pdf variables
        INVOICE_TEMPLATE_PATH = 'invoice_template.pdf'
        INVOICE_OUTPUT_PATH = 'invoice.pdf'
        
        
        ANNOT_KEY = '/Annots'
        ANNOT_FIELD_KEY = '/T'
        ANNOT_VAL_KEY = '/V'
        ANNOT_RECT_KEY = '/Rect'
        SUBTYPE_KEY = '/Subtype'
        WIDGET_SUBTYPE_KEY = '/Widget'

        data_dict = {
           'business_name_1': 'Bostata',
           'customer_name': 'company.io',
           'customer_email': 'joe@company.io',
           'invoice_number': '102394',
           'send_date': '2018-02-13',
           'due_date': '2018-03-13',
           'note_contents': 'Thank you for your business, Joe',
           'item_1': 'Data consulting services',
           'item_1_quantity': '10 hours',
           'item_1_price': '$200/hr',
           'item_1_amount': '$2000',
           'subtotal': '$2000',
           'tax': '0',
           'discounts': '0',
           'total': '$2000',
           'business_name_2': 'Bostata LLC',
           'business_email_address': 'hi@bostata.com',
           'business_phone_number': '(617) 930-4294'
        }
        write_fillable_pdf(INVOICE_TEMPLATE_PATH, INVOICE_OUTPUT_PATH, data_dict)

    def check_expiration_date(self, cr, uid, context=None):
        _logger.debug("Check_expiration_date START")
        member_line_obj = self.pool.get('res.partner')
        #Contains all ids for the model scheduler.demo
        member_line_ids = self.pool.get('res.partner').search(cr, uid, [])   
        #Loops over every record in the model scheduler.demo
        for member_line_id in member_line_ids :
            #Contains all details from the record in the variable member_line
            member_line =member_line_obj.browse(cr, uid,member_line_id ,context=context)
            if member_line.member:
                valid_membership = member_line.valid_membership
                onrenewal = False
                msg = "onrenewal = False"
                if valid_membership:
                    today = fields.Date.today()
                    expirdate = member_line.expirdate
                    todayyear,todaymonth,todayday = today.split("-")
                    expiryear,expirmonth,expirday = expirdate.split("-")
                    # This is the correct line
#                    if (int(todayyear) > int(expiryear)) and (int(todaymonth) < 5):
                    # This is the dubug line to DELETE for production
                    if (int(todayyear) > int(expiryear)) and (int(todaymonth) < 12):

                        onrenewal = True
                        msg = "onrenewal = True"
                    _logger.debug(msg)
                    #Update the record
                member_line_obj.write(cr, uid, member_line_id, {'onrenewal': (onrenewal)}, context=context)
        _logger.debug("Check_expiration_date END")

    @api.multi
    def send_mail_to_member(self):
        _logger.debug("send_mail_to_member START")
        # Find the e-mail template
        # template = self.env.ref('ngo_membership_management.membership_renewal')
        # You can also find the e-mail template like this:
        template = self.env['ir.model.data'].get_object('ngo_membership_management', 'membership_renewal')
 
        # Send out the e-mail template to the user
        self.env['mail.template'].browse(template.id).send_mail(self.id)
        _logger.debug("send_mail_to_member END")
        
    def expiration_mail_send(self, cr, uid, context=None):
        _logger.debug("expiration_mail_send START")
        member_line_obj = self.pool.get('res.partner')
        _logger.debug("expiration_mail_send Got member_line_obj")
        #Contains all ids for the model scheduler.demo
        member_line_ids = self.pool.get('res.partner').search(cr, uid, [])
        _logger.debug("expiration_mail_send Got member_line_ids")   
        #Loops over every record in the model scheduler.demo
        for member_line_id in member_line_ids:
            member_line = member_line_obj.browse(cr, uid,member_line_id ,context=context)
            _logger.debug("expiration_mail_send Got member_line")
            if member_line.member:
                _logger.debug("expiration_mail_send Is a member")
                mail_counter = 0
                onrenewal=member_line.onrenewal
                if onrenewal:
                    _logger.debug("expiration_mail_send Is om renewal")
                    mail_counter = member_line.mail_counter
                    today = fields.Date.today()
                    valid_membership = member_line.valid_membership
                    if (mail_counter == 0):
                        _logger.debug("expiration_mail_send Mail conter is 0")
                        _logger.debug("expiration_mail_send Before Call send_mail")
                        res_partner.send_mail_to_member(member_line)
                        _logger.debug("expiration_mail_send After Call send_mail")
                        mail_counter = mail_counter + 1
                        last_mail = today
                        _logger.debug("First mail")
                    else:
                        _logger.debug("expiration_mail_send Mail conter is >0")
                        date_format = '%Y-%m-%d'
                        current_date = (datetime.today()).strftime(date_format)
                        last_mail = fields.Datetime.from_string(member_line.last_mail)
                        d1 = datetime.strptime(member_line.last_mail, date_format).date()
                        d2 = datetime.strptime(current_date, date_format).date()
                        difference = (d2 - d1).days
                        _logger.debug(difference)
                        if (difference >= 28):
                            mail_counter = mail_counter + 1
                            last_mail = today
                            _logger.debug("MAIL No " + str(mail_counter))
                            _logger.debug("expiration_mail_send Before Call send_mail")
                            res_partner.send_mail_to_member(member_line)
                            _logger.debug("expiration_mail_send After Call send_mail")
                            if mail_counter == 4:
                                valid_membership = False
                                onrenewal = False
                    member_line_obj.write(cr, uid, member_line_id, {'mail_counter': (mail_counter)}, context=context)
                    member_line_obj.write(cr, uid, member_line_id, {'last_mail': (last_mail)}, context=context)
                    member_line_obj.write(cr, uid, member_line_id, {'valid_membership': (valid_membership)}, context=context)
                    member_line_obj.write(cr, uid, member_line_id, {'onrenewal': (onrenewal)}, context=context)
        _logger.debug("expiration_mail_send END")

# Class Section        
class Section(models.Model):
    _name = 'ngo_membership_management.section'
    
    name = fields.Char(string="Section", required=True)
    coordinator = fields.Many2one('res.partner', ondelete='set null', string="Coordinator", domain = "[('member','=',True)]", index=True)
    nmembers = fields.Integer(string="Number of members")
    openingdate = fields.Date(string="Opening Date")
    street = fields.Char(string="Address")
    zip = fields.Char(striong="Zip", size=24, change_default=True)
    state_id = fields.Many2one("res.country.state", string='State', help='Enter State', ondelete='restrict')
    city =fields.Char(string="City")
    phone = fields.Char(string="Phone")
    note =fields.Text()
