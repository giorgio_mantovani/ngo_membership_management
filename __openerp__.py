# -*- coding: utf-8 -*-
{
    'name': "ngo_membership_management",
    'summary': """Manage membership""",
    'version': '0.28',
    'description': """
        Memebership Management module for managing ngo members
            - Member Database
            - Annual fees collection
            - Memebership renewal reminders
    """,
    'license': 'AGPL-3',
    'author': "ISF ONLUS (Giorgio Mantovani)",
    'website': "http://www.informaticisenzafrontiere.org",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Management tools',

    # any module necessary for this one to work correctly
    'depends': ['base',
                 'board',
                 'mail',
                 'account',
                 'account_accountant',
                 'document',
                 'analytic',
                 'account_budget'],
    'external_dependencies': {"python" : ["PyPDF2"]},

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/templates.xml',
        'views/ngo_membership_management.xml',
        'views/auto_action.xml',
        'views/reminder_mail.xml',
        'data/sections.xml',
        'data/receipts.xml',
        'data/receipts_folder.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}